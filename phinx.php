<?php

global $config;
require_once 'config.php';

$db_config = [
    'adapter' => 'pgsql',
    'host' => $config['DATABASE_HOST'],
    'name' => $config['DATABASE_NAME'],
    'user' => $config['DATABASE_USER'],
    'pass' => $config['DATABASE_PASSWORD'],
    'port' => '5432',
    'charset' => 'utf8'
];

return
    [
        'paths' => [
            'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
            'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
        ],
        'environments' => [
            'default_migration_table' => 'phinxlog',
            'default_environment' => 'development',
            'production' => $db_config,
            'development' => $db_config,
            'testing' => $db_config
        ],
        'version_order' => 'creation'
    ];
