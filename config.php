<?php

$config = parse_ini_file('config.ini');

if (!$config) {
    die('Не удалось считать конфигурацию.');
}

$config['SESSIONS_TABLE_NAME'] = 'sessions';
