<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

function generate_date_pair(): array
{
    // От 2023-08-01 и до сегодня.
    $login_timestamp = mt_rand(1690833600, time() - 7 * 24 * 60 * 60);
    // Длительность сессии от 10 секунд до 7 дней.
    $session_length = mt_rand(10, 7 * 24 * 60 * 60);

    return [date('Y-m-d H:i:s', $login_timestamp), date('Y-m-d H:i:s', $login_timestamp + $session_length)];
}

function uuid4(): string
{
    $randomBytes = bin2hex(random_bytes(16));
    return sprintf('%s-%s-%s-%s-%s',
        substr($randomBytes, 0, 8),
        substr($randomBytes, 8, 4),
        substr($randomBytes, 12, 4),
        substr($randomBytes, 16, 4),
        substr($randomBytes, 20)
    );
}

class MockData extends AbstractSeed
{
    public function run(): void
    {
        // Итого 420 000 записей.
        // Если больше – может упасть с тайм-аутом (видимо долго выполянется транзакция).
        for ($j = 1; $j <= 20; $j++) {
            $values = array();
            $statement_parts = array();

            $statement = 'INSERT INTO sessions (user_id, login_time, logout_time) VALUES';

            for ($i = 1; $i <= 7000; $i++) {
                $uuid = uuid4();

                // Вставляем две записи с идентичным юзером, для разнообразия.
                array_push($values, $uuid, ...generate_date_pair());
                array_push($values, $uuid, ...generate_date_pair());
                array_push($values, uuid4(), ...generate_date_pair());

                array_push($statement_parts, '(?, ?, ?)', '(?, ?, ?)', '(?, ?, ?)');

                // Добавляем на 1500 обычных сессий одну незавершенную.
                if (is_int($i / 500)) {
                    array_push($values, $uuid, generate_date_pair()[0], null);
                    $statement_parts[] = '(?, ?, ?)';
                }
            }

            $this->execute($statement . implode(', ', $statement_parts), $values);

            printf("Запланировано записей на вставку: %d\n", $j * 7000 * 3);
        }
    }
}
