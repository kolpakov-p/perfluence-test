<?php

declare(strict_types=1);

require_once 'config.php';

use Phinx\Migration\AbstractMigration;

final class Initial extends AbstractMigration
{
    public function change(): void
    {
        global $config;

        // TODO: Индексы?

        $table = $this->table($config['SESSIONS_TABLE_NAME']);
        $table->addColumn('user_id', 'uuid')
            ->addColumn('login_time', 'timestamp', [
                'timezone' => false
            ])
            ->addColumn('logout_time', 'timestamp', [
                'timezone' => false
            ])
            ->create();
    }
}
