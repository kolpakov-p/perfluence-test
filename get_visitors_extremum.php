<?php

global $config;
require_once 'config.php';

/**
 * Обрабатываем строку-результат запроса из базы данных и изменяем результирующую таблицу (на месте).
 * @throws Exception
 */
function process_database_record($row, &$resulting_table): void
{
    $login_time = (new DateTime($row['login_time']))->getTimestamp();
    $logout_time = $row['logout_time'] ? (new DateTime($row['logout_time']))->getTimestamp() : INF;

    $corresponding_record_key = null;
    $corresponding_record = null;

    // Ищем запись, период которой пересекается с текущей (хотя бы частично).
    foreach ($resulting_table as $key => $value) {
        if (!($login_time >= $value['end_time'] || $logout_time <= $value['start_time'])) {
            $corresponding_record_key = $key;
            $corresponding_record = $value;
            break;
        }
    }

    // Если текущая запись не вписывается в период ни одной из уже имеющихся – добавляем новую в результирующую таблицу.
    if (!$corresponding_record) {
        $resulting_table[] = ['start_time' => $login_time, 'end_time' => $logout_time, 'count' => 1];
        return;
    }

    // Увеличиваем счётчик существующей записи, ведь мы нашли ту самую, в период которой мы попадаем.
    $resulting_table[$corresponding_record_key]['count'] = $corresponding_record['count'] + 1;

    //region Сужаем диапазон, если новая запись у́же, чем предыдущая.
    if ($login_time > $corresponding_record['start_time']) {
        $resulting_table[$corresponding_record_key]['start_time'] = $login_time;
    }

    if ($logout_time < $corresponding_record['end_time']) {
        $resulting_table[$corresponding_record_key]['end_time'] = $logout_time;
    }
    //endregion
}

// Формат даты для подстановки в базу данных.
const DB_DATE_FORMAT = "Y-m-d H:i:s";
// Будем выгребать из базы по 10 000 записей за раз.
const DB_LIMIT = 10000;
// Инициализирует курсор для пагинации.
$db_cursor = 0;

$day = @$argv[1];

if (!$day) {
    die('Первым аргументом необходимо передать дату в формате Y-m-d («2023-09-02»).');
}

try {
    $start_of_day = new DateTime($day);
} catch (Exception $e) {
    die('Произошло что-то страшное при разборе переданной даты: ' . $e);
}

$end_of_day = (new DateTime())->setTimestamp($start_of_day->getTimestamp() + 86400);

try {
    $pdo = new PDO(sprintf('pgsql:dbname=%s;host=%s', $config['DATABASE_NAME'], $config['DATABASE_HOST']), $config['DATABASE_USER'], $config['DATABASE_PASSWORD'], [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ]);
} catch (PDOException $e) {
    die('Подключение не удалось: ' . $e->getMessage());
}

$resulting_table = [];

$stmt = $pdo->prepare("
    SELECT id, login_time, logout_time FROM sessions
    WHERE login_time < ?
      AND (logout_time IS NULL OR  logout_time > ?)
      AND id > ?
    ORDER BY id ASC
    LIMIT ?
");

do {
    $stmt->execute([
        $end_of_day->format(DB_DATE_FORMAT),
        $start_of_day->format(DB_DATE_FORMAT),
        $db_cursor, DB_LIMIT
    ]);

    // Прекращаем цикл если достигли конца.
    if ($stmt->rowCount() === 0) {
        break;
    }

    while ($row = $stmt->fetch()) {
        try {
            process_database_record($row, $resulting_table);
        } catch (Exception $e) {
            die("Что-то сломалось при обработке записи из базы данных: " . $e);
        }

        // Переставляем курсор пагинации.
        // Не очень оптимально, но уже мелочи.
        $db_cursor = $row['id'];
    }
} while (true);

usort($resulting_table, function ($a, $b) {
    return $b['count'] - $a['count'];
});

printf("За %s были следующие периоды с пиковыми нагрузками:\n\n", $day);

for ($i = 0; $i < (min(count($resulting_table), 5)); $i++) {
    $entry = $resulting_table[$i];

    $period_start = (new DateTime())->setTimestamp($entry['start_time'])->format("H:i:s");
    $period_end = (new DateTime())->setTimestamp($entry['end_time'])->format("H:i:s");

    printf("C %s по %s с %d одновременными сессиями.\n", $period_start, $period_end, $entry['count']);
}
